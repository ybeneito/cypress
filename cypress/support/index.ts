/// <reference types="cypress" />

declare namespace Cypress { 
    interface Chainable {  
        /**
         * login with the credentials from the fixture
         * @example cy.login()
         */
        login(): void
        
        /**
         * close and open sidebar
         * @example cy.openCloseSidebar()
         */
        openCloseSidebar (): void

        /**
         * filter ward names
         * @param wardName the ward name to filter
         * @example cy.filterWardName("ali")
         */
        filterByWardName(wardName: string): void

        /**
         * navigate by dropdown
         * @param dropdown the dropdown name use for navigate
         * @param select the item selected name use for navigate
         * @example cy.navByDropdown("GED","Documents")
         */
        navByDropdown(dropdown: string, select: string): void

        /**
         * search in a table
         * @param table the selected teble
         * @param input the input to search
         * @example cy.searchTable("banks","place")
         */
        tableSearch(table:string, input: string): void

        /**
         * search in a table, fail and show error message
         * @param table the selected teble
         * @param input the input to search
         * @example cy.searchTable("banks","blablablabla")
         */
        tableSearchFail(table:string, input: string): void

        resetTableSearch(): void

        /**
         * sort rows by a column
         * @param column the column to sort
         * @example cy.sortBy("name")
         */
        sortBy(column: string): void
        
        /**
         * use pagination 
         */
        paginate(): void


    }
}


