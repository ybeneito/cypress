const { _ } = Cypress

const tableSearch = (table: string, input: string) => { 
    /*
    cy.get(`#search-${table}-input`).type(`${input}{enter}`).wait(500);
    cy.get(".mat-row").should("contain", input);
    */
    cy.get('.search_input_container > .search_form_input > .search_input').type(`${input}{enter}`).wait(500);
    cy.get(".mat-row").should("contain", input);
}
Cypress.Commands.add("tableSearch", tableSearch);

const tableSearchFail = (table: string, input: string) => {
    // cy.get(`#search-${table}-input`).type(`${input} {enter}`).wait(500);
    cy.get('.search_input_container > .search_form_input > .search_input').type(`${input}{enter}`).wait(500);
    // cy.get(`.card_no_${table}`).should("be.visible");
    cy.get('.mat-list-text > .mat-line').should("contain.text", table)
    cy.get('.search_input_container > .ng-star-inserted').click()
    // cy.get("#reset_search_grid").click();
}
Cypress.Commands.add("tableSearchFail", tableSearchFail); 

const resetTableSearch = () => {
    /*
    cy.get("#reset_search_grid").click().wait(500)
    */
    cy.get('.search_input_container > .ng-star-inserted').click().wait(500)
}
Cypress.Commands.add("resetTableSearch", resetTableSearch);

const sortBy = (column: string) => { 
    cy.get(`.mat-table > mat-row >.cdk-column-${column}`).then((values) => { 
        const sortedCy = _.sortBy(values, 'innerText')
        cy.log(sortedCy[0].innerText)
        cy.get('.mat-header-row > .cdk-column-' + column).click().wait(1000)
        cy.get(`.mat-table > mat-row >.cdk-column-${column}`).then((sorted) => { 
            expect(sorted[0].innerText).to.be.equal(sortedCy[0].innerText)
        })
    })
}
Cypress.Commands.add("sortBy", sortBy);

const paginate = () => { 
    cy.get('.mat-button > .mat-button-wrapper').should("contain.text", "Page 1")
    cy.get(':nth-child(4) > .mat-button-wrapper > .mat-icon').click().wait(1000)
    cy.get('.mat-button > .mat-button-wrapper').should("contain.text", "Page 2")
    cy.get(':nth-child(2) > .mat-button-wrapper > .mat-icon').click().wait(1000)
    cy.get('.mat-button > .mat-button-wrapper').should("contain.text", "Page 1")
}

Cypress.Commands.add("paginate", paginate);