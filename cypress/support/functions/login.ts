const login = () => {
    cy.fixture("credentials").as("credent");
    cy.get("@credent").then((c: any) => {
      cy.session([c.username, c.password], () => { 
        cy.visit("/login");
        cy.get('input[placeholder*="identifiant / email"]').type(c.username);
        cy.get('input[placeholder*="mot de passe"]').type(c.password);
        cy.get("#btn").click();
        cy.url().should("include", "document");
      });
    });
}

Cypress.Commands.add("login", login);