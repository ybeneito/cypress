const openCloseSidebar = () => { 
    cy.get("ward-name-list > .mat-nav-list").children().should("have.length.greaterThan", 0);
    cy.get('.sidebar_toggle').click().wait(500);
    cy.get("ward-name-list > .mat-nav-list").should("be.visible");
    cy.get(".sidebar_toggle").click();
    cy.get("ward-name-list > .mat-nav-list").should("not.be.visible");
}
Cypress.Commands.add("openCloseSidebar", openCloseSidebar);

const filterByWardName = (wardName: string) => { 
    cy.get('.sidebar_toggle').click().wait(500);
    cy.get("#sidebar_search").type(`${wardName}{enter}`);
    cy.get("ward-name-list > .mat-nav-list > a").each((e) => {
        expect(e.text().toLowerCase()).contains(wardName);
    });
    cy.get(".nb-form-field-suffix-medium").click();
}
Cypress.Commands.add("filterByWardName", filterByWardName);

const navByDropdown = (dropdown: string, select: string ) => { 
    cy.get(`custom-menu`).each((e) => { 
        if (e.text().toLowerCase().includes(dropdown.toLowerCase())) { 
            cy.wrap(e).click().wait(500);
        }})
        
    cy.get(`.mat-line`).each((e) => { 
        if (e.text().toLowerCase().includes(select.toLowerCase())) { 
            cy.wrap(e).click().wait(500);
        }
    })
    cy.url().should("contain",dropdown.toLowerCase());
}
Cypress.Commands.add("navByDropdown", navByDropdown);


