import '../../support/functions/login'
import '../../support/functions/table'
import '../../support/functions/layout'

describe("Test with shareds", () => {
  before(() => {
    cy.intercept(Cypress.env("apiUrl") + "/documents/shares?limit=20").as("getShareds");
  });

  beforeEach("login or use current session", () => {
    cy.login();
    cy.visit("/ged/documents");
    cy.navByDropdown("ged", "partages")
  });
  // V�rifie l'affichage des partages de documents
  it("should display a list of shareds", () => {
    cy.wait("@getShareds").then((intercept) => {
      cy.url().should("contain", "partages");
      cy.get("mat-table > mat-row").should("have.length", intercept.response.body.length);
      cy.get("mat-table > mat-row").first().should("contain", intercept.response.body[0].name);
    });
  });
  // Comme pour les documents besoin de modifier un peu la table
  it("should use table tests", () => {
    // cy.paginate()
    cy.tableSearch("partages", "31")
    cy.resetTableSearch()
    cy.tableSearchFail("partages", "blablabla")
    cy.sortBy("name")
  })
});
