import '../../support/functions/login'
import '../../support/functions/layout'

describe("testing if the layout is correct", () => {  
    beforeEach("login or use current session", () => {
      cy.login();
      cy.visit("/ged/documents");
    });
  
    // Vérification de l'affichage de la sidebar avec minimum 1 ward et test sur le toggle
    it("should manipulate sidebar", () => {
      cy.openCloseSidebar()
    });
  
    // Vérifications sur la barre de recherche des wards
    // TODO à fix, pour l'instant rien ne déclanche la recherche
    it.skip("should filter by ward name with the search bar", () => {
      cy.filterByWardName("ali");
    });
  
    // Vérification du comportement du dropdown menu GED passage partages/documents
    it("should redirect the asked page by dropdown menu", () => {
      cy.navByDropdown("Contacts", "Tribunaux");
    });
  
    // Vérification du comportement barre du haut et d'apparition et fermeture des onglets
    it.only("should display a tab when you call for shared or ward page", () => {
      cy.get('.sidebar_toggle').click().wait(500);
      // ouverture de l'onglet 1er protégé
      cy.get("ward-name-list > .mat-nav-list > :nth-child(1) > .mat-list-item-content").click();
      cy.get(".dock_tab_container").should("have.length", 2);
      cy.url().should("contain", "protege");
      
      // fermeture de l'onglet 1er protégé
      cy.get('dock-sub-tab-ward > * > * > mat-icon').click()
      cy.get(".dock_tab_container").should("have.length", 1);

      // changement de page et nouvel onglet
      cy.navByDropdown("Contacts", "Tribunaux");

      // ouverture de plusieurs d'onglets
      cy.get("ward-name-list > .mat-nav-list > :nth-child(1) > .mat-list-item-content").click().wait(500);
      cy.get("ward-name-list > .mat-nav-list > :nth-child(2) > .mat-list-item-content").click().wait(500);
      cy.get("ward-name-list > .mat-nav-list > :nth-child(3) > .mat-list-item-content").click().wait(500);
      cy.get("ward-name-list > .mat-nav-list > :nth-child(4) > .mat-list-item-content").click().wait(500);
      cy.get("ward-name-list > .mat-nav-list > :nth-child(5) > .mat-list-item-content").click().wait(500);
      cy.get(".dock_tab_container").should("have.length", 7);

      // fermeture de tous
      cy.get(':nth-child(3) > .btn_popover_tabs > .icon_popover_tabs > custom-menu > .custom_menu_btn_container').click()
      cy.get(".all_tabs_tab_container > .mat-icon").click()
      cy.get(".dock_tab_container").should("have.length", 1);
    });
  });
  