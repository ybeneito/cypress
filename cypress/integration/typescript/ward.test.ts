import "../../support/functions/login";

describe("Tests for the ward page", () => {
    let currentWardId;
  
    beforeEach("login or use current session", () => {
      cy.login();
      cy.visit("/ged/documents");
      cy.get('.sidebar_toggle').click().wait(500);
    });
  
    //vérification que au moins un ward est affiché dans la liste
    it("should display ward's list in sidebar", () => {
      cy.get("ward-name-list > .mat-nav-list").children().should("have.length.greaterThan", 0);
    });
  
    //vérification de la cohérence du ward affiché/demandé    
    it("should give ward's info when user click on him", () => {
      cy.get("ward-name-list > .mat-nav-list > :nth-child(1) > .mat-list-item-content").click();
      // intercepte la requête des infos d'un ward et de ses comptes
      cy.intercept(Cypress.env("apiUrl") + "/wards/*").as("getWardInfos");
      cy.intercept(Cypress.env("apiUrl") + "/wards/*****/ward-bank-accounts").as("getBank");
      // ici on récupére la réponse et on vérifie que le ward reçu est le bon via son id
      cy.wait("@getWardInfos").then(({ response }) => {
        if(response.body.id) {
        currentWardId = response.body.id;
      }
        cy.url().should("contain", response.body.id);
        // et que tout s'affiche en fonction de la réponse
        // en premier le nom du ward courant
        cy.get('.ward_sheet_personal_img_container > .full_width > .card-title').should("contain", response.body.name);
        // ensuite on vérifie que le blocnote contient sa note
        cy.get('.textarea').invoke("val").should("eq", response.body.notepad);
        // et que l'adresse affichée soit bonne
        cy.get('personal-info-cell > .mat-card > :nth-child(2)').should("contain", response.body.coordinates.mainAddress.city);
      });
      cy.wait("@getBank").then(({ response }) => {
        cy.get('.ward_sheet_cell_list > :nth-child(1)').should("contain", response.body[0].bankName);
        cy.get('.ward_sheet_banks_cell > .ward_sheet_cell_header').should("contain", response.body.length);
      });
    });
  
    // test du passage de ward page => ward/id/document
    it("should get redirect on documents page when user click on btn", () => {
      cy.get("ward-name-list > .mat-nav-list > :nth-child(1) > .mat-list-item-content").click();
      cy.get('.ward_sheet_personal_img_container > .full_width > .card-text > .mat-focus-indicator').click();
      cy.url().should("contain", currentWardId);
    });
  
    // Test de modification des notes en ajoutant le char "t" et en le retirant pour pas poluer
    it("should change the note in ward page", () => {
      cy.intercept(Cypress.env("apiUrl") + "/wards/*").as("getWardInfos");
      cy.get("ward-name-list > .mat-nav-list > :nth-child(1) > .mat-list-item-content").click();
      cy.wait("@getWardInfos").then(({ response }) => {
        cy.get('.textarea')
          .click()
          .type("t")
          .invoke("val")
          .should("eq", response.body.notepad + "t");
          cy.get('.textarea').click().type("{backspace}").invoke("val").should("eq", response.body.notepad);
      });
    });
  });
  