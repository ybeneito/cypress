import "../../support/functions/table"
import "../../support/functions/login"

describe("Test for documents list", () => {
  before("Login in app", () => {
    let firstDocument;
    let fix;
    cy.login();
    cy.fixture("documents").then((fixDocs) => {
      fix = fixDocs
    })
    cy.intercept(Cypress.env("apiUrl") + "/documents?limit=20", (req) => {
      req.reply({
        headers: {
          "accept-range": "documents 100",
          "access-control-expose-headers": "WWW-Authenticate, Content-Range, Accept-Range, Link",
          "link": `[<https://localhost:8443/v2/api/documents?limit=20&offset=20>; rel="next", <https://localhost:8443/v2/api/documents?limit=20&offset=0>; rel="first", <https://localhost:8443/v2/api/documents?limit=20&offset=80>; rel="last"]`,
          "content-range": "0-20/821",
          "content-type": "application/json",
        },
        body: fix
      })
    }).as("getDocs")
    cy.visit("/ged/documents")
    cy.url().should("contain", "documents");
  });

  beforeEach("login or use current session", () => {
    cy.login();
    cy.visit("/ged/documents");
  });


  it("should display a list of 20 docs from the fixtures", () => {
    cy.wait("@getDocs")
    cy.get(':nth-child(2) > .cdk-column-wardName').should("be.visible")
    cy.get(':nth-child(2) > .cdk-column-wardName').should("contain", "fixtured")
  });

  //TODO 
  /*
  it("should filter with keywords", () => {
    cy.intercept(Cypress.env("apiUrl") + "/documents?offset=0&limit=20&search=((keywords%3Dlike%3D%27*BANQUE*%27))").as("askedFilter");
    cy.get(":nth-child(2) > .keyword_chip").click();
    cy.wait("@askedFilter").then(({ response }) => {
      cy.get('grid-navigation.ng-star-inserted > .container-fluid > .row > .col > .ng-star-inserted').should("contain", response.body.length);
    });
    cy.get(".m-l-3 > .keyword_icon").click().wait(500);
  });
  */

  // Vérifie si le document affiché dans la boite de dialogue porte le bon nom
  it("should display bottom sheet when user click on (i) btn", () => {
    cy.get(':nth-child(3) > .cdk-column-name').then((name) => {
      cy.wait(500)
      cy.get('#mat-chip-list-2 > .mat-chip-list-wrapper > :nth-child(5) > .mat-icon').click().wait(500)
      cy.get('.mat-card.m-0 > .h-p-100').should("contain", name.text());
      cy.get('[style="width: 50%; text-align: end;"] > .mat-focus-indicator > .mat-button-wrapper > .mat-icon').click()
      cy.get('.mat-card.m-0 > .h-p-100').should("not.exist");
    })
  });

  // Vérifie si l'utilisateur peut selectionner et deselectionner des documents
  it("should show to user asked documents in dialog and unselect by clicking btn", () => {
    cy.get(":nth-child(2) > .cdk-column-name").then((name) => {
      cy.get('#mat-checkbox-2 > .mat-checkbox-layout > .mat-checkbox-inner-container').click();
      cy.get('.window_selection_list_item').should("contain", name.text());
      cy.get(':nth-child(2) > .cdk-column-select > mat-checkbox').click();
      cy.get('.window_selection_list_item').should("not.contain", name.text());
      cy.get('.window_selection_header_actions > .mat-icon-no-color').click();
      cy.get('.window_selection_list_item').should("not.exist");
    })
  });

  // Vérifie la redirection vers les partages et retour
  it("should redirect on document shareds", () => {
    cy.get('.web_app_tabs_container > .cursor_pointer').click();
    cy.url().should("contain", "partages");
    cy.get('.web_app_tabs_container > .cursor_pointer').click();
    cy.url().should("contain", "documents");
  });

  // Utilisation des tests de table utilisation de search avant sort afin de réduire la liste des documents
  it("should use table tests", () => {
    cy.paginate()
    cy.tableSearch("document","impôts")
    cy.sortBy("name")
    cy.resetTableSearch()
    cy.tableSearchFail("document","blablabla")
  })
});
