import "../../support/functions/login";
import "../../support/functions/table";

describe("Tests on contacts pages", () => {
  before(() => {
    // Context
    let fix;
    let sorted;
    cy.fixture("banks").then((fixBanks) => {
      fix = fixBanks
    })
    cy.fixture("sortedBanks").then((fixSorted) => {
      sorted = fixSorted
    })
    cy.intercept(Cypress.env("apiUrl") + "/contacts/banks?limit=20", (req) => {
      req.reply({
        headers: {
          "accept-range": "contactsBanks 25",
          "access-control-expose-headers": "WWW-Authenticate, Content-Range, Accept-Range, Link",
          "link": `[<${Cypress.env("apiUrl")}/contactbank?limit=20&offset=20>; rel="next", <https://localhost:8443/v2/api/contactbank?limit=20&offset=0>; rel="first", <https://localhost:8443/v2/api/contactbank?limit=20&offset=80>; rel="last"]`,
          "content-range": "0-20/99",
          "content-type": "application/json",
        },
        body: fix
      })
    }).as("getBanks")
  });
  
  beforeEach("login or use current session", () => {
    cy.login();
    cy.visit("/contacts/banks")
    cy.url().should("contain", "banks");
  });
  

  it("should display a list of 20 banks from the fixtures", () => {
    cy.wait("@getBanks");
    cy.get(':nth-child(2) > .cdk-column-name').should("be.visible")
    cy.get('.mat-table > :nth-child(2) > .cdk-column-name').should("contain", "Fixtured")
  });
  
  // This one is a little tricky, at first we want to reduce the number of banks chosen way is to filter them by name with "place"
  // Now it's easier with 4 items we don't care about pagination. we can select the cells in this state send them to Cypress.sortBy() return is send in sortedCy.
  // Now we can ask for sorting, wait for res and check cells.textContent value with sortedCy.
  // In the end we should reset the displayed list by undo the filter.
  it("should use table tests", () => {
    cy.tableSearch("contact bancaire","postale")
    cy.sortBy("name")
    cy.resetTableSearch()
    cy.paginate()
    cy.tableSearchFail("contact bancaire","blablabla")
  })

 it("should go on courts contact and do table tests", () => { 
   // cy.get(".contact_page_global_tab:nth-child(3)").click()
   cy.get('.web_app_tabs_container > :nth-child(3)').click()
   cy.url().should("contain", "courts");
    cy.tableSearch("courts","agen")
    cy.sortBy("name")
    cy.resetTableSearch()
    // cy.paginate()
    ///TODO besoin d'ajouter le card-no_courts
    // cy.tableSearchFail("courts","blablabla")
  
   })

   it.skip("should open modal, adding a contact and search for new entrie", () => {
    cy.intercept('POST', Cypress.env("apiUrl") + "/contacts/banks").as("postBank")
      cy.get('.grid-menu-btn').click()
      cy.get('#mat-input-0').type("Cypress Bank")
      cy.get('.mat-select-placeholder').click()
      cy.get('#mat-option-0 > .mat-option-text').click()
      cy.get('#mat-input-1').type("1234")
      cy.get('#mat-input-3').type("FR")
      cy.get('#mat-input-4').type("123")
      cy.get('#mat-input-5').type("12345")
      cy.get('#mat-input-6').type("12345")
      cy.get('#mat-input-7').type("2 rue de la rue")
      cy.get('#mat-input-8').type("34000")
      cy.get('#mat-input-9').type("Montpellier")
      cy.get('#mat-input-11').type("0123456789")
      cy.get('#mat-input-12').type("9876543210")
      cy.get('#mat-input-13').type("0123456789")
      cy.get('#mat-input-14').type("9876543210")
      cy.get('#mat-input-15').type("test@test.fr")
      cy.get('#mat-input-16').type("www.site.fr")
      cy.contains('Créer').click()
      cy.wait("@postBank").its("response.statusCode").should("eq", 201)
      cy.get('.mat-snack-bar-container').should("contain", "Le contact a bien été ajouté")
      cy.tableSearch("banks","Cypress Bank")
      cy.get('.mat-row > .cdk-column-name').should("contain", "Cypress Bank")

   }) 
 })
