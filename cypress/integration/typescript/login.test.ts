import '../../support/functions/login'

describe("Test on login page", () => {
    beforeEach("Redict to login page", () => {
      cy.fixture("credentials").as("credent");
    });
  
    it("should display error message if credentials are wrong", () => {
      cy.get("@credent").then((c: any) => {
        cy.visit("/login");
        cy.get('input[placeholder*="identifiant / email"]').type(c.username + "test");
        cy.get('input[placeholder*="mot de passe"]').type(c.password);
        cy.get("#btn").click();
        cy.contains("pas valide");
      });
    });
  
    it("Should connect with good credentials and redirect user on logout", () => {
      Cypress.session.clearAllSavedSessions()
      cy.get("@credent").then((c: any) => {
          cy.visit("/login");
          cy.get('input[placeholder*="identifiant / email"]').type(c.username);
          cy.get('input[placeholder*="mot de passe"]').type(c.password);
          cy.get("#btn").click();
          cy.url().should("include", "document");
          cy.get('.custom_menu_btn_container > .mat-line').click()
          cy.url().should("include", "login");
      });
    });
  });
  
  
  